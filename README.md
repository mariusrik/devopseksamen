# DevOps eksamen 2017

Continuous deployment med Jenkins og Kubernetes <br/>
Hensikten med prosjektet er ikke applikasjone, men devops.

Applikasjonen ligger her http://35.227.50.183/
Dokumentasjon til oppgaven ligger også her.

###Aplikasjonen
Appliksjonen er ment som en demo app fir deployment i kubernetes med jenkins. Jeg ville både ha backend og frontend i setupet.
<br>
Flaggen til applikasjonen her:

* port: Dette er porten http servern vil lytte på
* endtype: Enten “frontend” eller “backend”.
* backend: Denne trengs bare om typen er frontend slik at frontend peker på backend.
<br>

Eksempel på å kjøre applikasjone med node:
<br>
Backend:
```bash
$ node server.js --endtype=backend
``` 
For å starte frontend serveren slik at den kan kommunisere med serveren over:
```bash
$ node server.js --endtype=frontend --port=3001 --backend=http://127.0.0.1:8080```  
```
<br>

Appen ser slik ut:
Den viser fram informasjon om hvilken container den er i, app versjon og prosjektnavn.
![App info](images/appinfo.png)


Skisse over Jenkins deployment prosessen:
![App info](images/skisse.jpg)


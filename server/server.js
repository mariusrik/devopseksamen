const flags = require("node-flags");
let PORT = flags.get('port');
let TYPE = flags.get('endtype');
let BACKEND = flags.get('backend');


let port = PORT || 8080;
const app = require("./app")(TYPE,BACKEND);

if (app === null) {
	console.log("THAT TYPE IS NOT SUPPORTED!");
} else {
	app.listen(port, () => console.log("listening on port " + port));
}


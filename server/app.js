const express = require('express');
const cors = require('cors');
const os = require('os');
const bodyParser = require('body-parser');
const request = require("request");

module.exports = function (endtype, backend) {
  if (endtype === "backend") {
    return backend_server();
  } else if (endtype === "frontend") {
    return frontend_server(backend);
  } else {
//		console.log("FAIL");
    return null;
  }
}


function backend_server() {
  const app = express();
  app.use('/', cors());
  const pjson = require('./package.json');


  app.use(bodyParser.json());

  app.get(
    '/',
    (req, res) => res.send('Hello!')
  );


  app.get('/statusinfo', (req, res) => {
    console.log("test");
    //security risk, only for demo prod

    let jsonresponse = {
      projectname: pjson.name,
      containerid: os.hostname(),
      version: pjson.version,
    };

    res.status(200).send(JSON.stringify(jsonresponse));
  });


  app.get('/version', (req, res) => {
    res.status(200).send(pjson.version);
  });
  app.get('/containerid', (req, res) => {
    res.status(200).send(os.hostname());
  });

  app.get("/healthz", (req, res) => {
    res.status(200).send("OK");
  });

  return app;

}

function frontend_server(backend) {
  const mustacheExpress = require('mustache-express');

  const app = express();


  app.engine('html', mustacheExpress());
  app.set('view engine', 'html');

  app.set('views', __dirname + '/templates');

  app.use('/', cors());


  app.use(bodyParser.json());


  app.get(
    '/',
    (req, res) => {
      request(backend + "/statusinfo", function (error, response, body) {
        if (error) {
          console.log(error);
          res.status(500).send(error);
        } else {
          res.render('index', JSON.parse(body));
        }
      });
    }
  );



  app.get('/version', (req, res) => {
    request(backend + "/version", function (error, response, body) {
      if (error) {
        res.status(500).send(error);
      } else {
        let result = JSON.parse(body).version;
        res.status(200).send(result);
      }
    });
  });

  app.get("/healthz", (req, res) => {
    res.status(200).send("OK");
  });


  return app;
}
